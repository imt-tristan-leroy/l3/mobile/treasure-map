import 'package:sqflite/sqflite.dart';
import 'package:treasure_map/models/place.dart';

class DatabaseHandler {
  int version = 1;
  Database? db;

  static final DatabaseHandler _singleton = DatabaseHandler._internal();

  factory DatabaseHandler() {
    return _singleton;
  }

  DatabaseHandler._internal();

  Future<Database?> openDb() async {
    db ??= await openDatabase("treasures.db", version: version,
        onCreate: (Database db, int version) async {
      await db.execute('''
          create table Place ( 
            id integer primary key autoincrement, 
            designation text not null, 
            latitude double not null, 
            longitude double not null, 
            imageUrl text
          )
        ''');
    });
    return db;
  }

  Future loadPlace() async {
    db = await openDb();
    List<Map<String, dynamic>> places = [
      {
        'designation': 'IMT Nord Europe Villeneuve d\'Ascq',
        'latitude': 50.611277,
        'longitude': 3.14862
      },
      {
        'designation': 'IMT Nord Europe Bourseul',
        'latitude': 50.375807,
        'longitude': 3.067043
      },
      {
        'designation': 'IMT Nord Europe Lahure',
        'latitude': 50.386829,
        'longitude': 3.082074
      },
      {
        'designation': 'Stade Pierre Mauroy Lille',
        'latitude': 50.613192,
        'longitude': 3.129986
      },
      {
        'designation': 'Gare Lille Europe',
        'latitude': 50.639063,
        'longitude': 3.076309
      },
      {
        'designation': 'Grand Palais Lille',
        'latitude': 50.632431,
        'longitude': 3.076755
      },
    ];
    for (var place in places) {
      print('try to insert: $place');
      Place? p = await insertPlace(Place.fromMap(place));
      print('inserted: $p');
    }
  }

  Future<Place?> insertPlace(Place place) async {
    place.id = await db!.insert("Place", place.toMap());
    return place;
  }

  Future<List<Place>> getPlaces() async {
    List<Map<String, dynamic>> maps = await db!.query('Place');
    return maps.map((e) => Place.fromMap(e)).toList();
  }

  Future<int> deletePlace(int id) async {
    return await db!.delete("Place", where: 'id = ?', whereArgs: [id]);
  }

  Future<int> updatePlace(Place place) async {
    return await db!
        .update("Place", place.toMap(), where: 'id = ?', whereArgs: [place.id]);
  }
}
