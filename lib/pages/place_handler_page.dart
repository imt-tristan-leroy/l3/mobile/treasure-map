import 'package:flutter/material.dart';
import 'package:treasure_map/pages/place_list.dart';

class PlaceHandlerPage extends StatelessWidget {
  const PlaceHandlerPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Handle Place"),
      ),
      body: const PlaceList(),
    );
  }
}
