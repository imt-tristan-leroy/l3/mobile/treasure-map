import 'dart:io';

import 'package:flutter/material.dart';
import 'package:treasure_map/common/database_handler.dart';

import '../models/place.dart';

class ImagePage extends StatelessWidget {
  final String imagePath;
  final Place place;

  const ImagePage({
    Key? key,
    required this.imagePath,
    required this.place,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    DatabaseHandler dbHandler = DatabaseHandler();

    return Scaffold(
      appBar: AppBar(
        title: const Text('Image preview'),
        actions: [
          IconButton(onPressed: () {
            place.imageUrl = imagePath;
            dbHandler.updatePlace(place);
            Navigator.of(context).popUntil((route) => route.isFirst);
          }, icon: const Icon(Icons.save))
        ],
      ),
      body: Image.file(File(imagePath)),
    );
  }
}
