import 'package:flutter/material.dart';
import 'package:treasure_map/common/database_handler.dart';
import 'package:treasure_map/pages/place_dialog.dart';

import '../models/place.dart';

class PlaceList extends StatefulWidget {
  const PlaceList({Key? key}) : super(key: key);

  @override
  State<PlaceList> createState() => _PlaceListState();
}

class _PlaceListState extends State<PlaceList> {
  DatabaseHandler dbHandler = DatabaseHandler();

  late List<Place> _places = [];

  @override
  initState() {
    super.initState();
    dbHandler.getPlaces().then((places) => setState(() {
          _places = places;
        }));
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: _places.length,
        itemBuilder: (context, index) {
          return Dismissible(
              key: Key(_places[index].id.toString()),
              onDismissed: (key) {
                dbHandler.deletePlace(_places[index].id ?? -1);
                dbHandler.getPlaces().then((value) => setState(() {
                      _places = value;
                    }));
              },
              child: ListTile(
                title: Text(_places[index].designation ?? ""),
                trailing: IconButton(
                  onPressed: () {
                    PlaceDialog dialog =
                        PlaceDialog(isNew: false, place: _places[index]);
                    showDialog(
                        context: context,
                        builder: (context) {
                          return dialog.buildDialog(context);
                        }).then((value) => {
                          dbHandler.getPlaces().then((value) => setState(() {
                                _places = value;
                              }))
                        });
                  },
                  icon: const Icon(Icons.edit),
                ),
              ));
        });
  }
}
