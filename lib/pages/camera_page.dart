import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:treasure_map/pages/image_page.dart';

import '../models/place.dart';

class CameraPage extends StatefulWidget {
  final Place place;

  const CameraPage({Key? key, required this.place}) : super(key: key);

  @override
  State<CameraPage> createState() => _CameraPageState(place: place);
}

class _CameraPageState extends State<CameraPage> {
  final Place place;
  CameraController? _controller;
  late Image image;

  _CameraPageState({required this.place});

  @override
  void initState() {
    super.initState();
    CameraController controller;
    availableCameras().then((cameras) => {
          controller = CameraController(cameras[0], ResolutionPreset.medium),
          controller.initialize().then((_) {
            if (!mounted) {
              return;
            }
            setState(() {
              print("Init");
              _controller = controller;
            });
          }).catchError((Object e) {
            if (e is CameraException) {
              switch (e.code) {
                case 'CameraAccessDenied':
                  print('User denied camera access.');
                  break;
                default:
                  print('Handle other errors.');
                  break;
              }
            }
          }),
        });
  }

  @override
  void dispose() {
    super.dispose();
    _controller!.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Take a picture"),
        actions: [
          IconButton(
              onPressed: () {
                _controller!.takePicture().then((file) => {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) =>
                              ImagePage(imagePath: file.path, place: place))),
                    });
              },
              icon: const Icon(Icons.camera))
        ],
      ),
      body: _controller == null ? Container() : CameraPreview(_controller!),
    );
  }
}
