import 'dart:io';

import 'package:flutter/material.dart';
import 'package:treasure_map/common/database_handler.dart';
import 'package:treasure_map/pages/camera_page.dart';

import '../models/place.dart';

class PlaceDialog {
  final TextEditingController txtDes = TextEditingController();
  final TextEditingController txtlat = TextEditingController();
  final TextEditingController txtlon = TextEditingController();
  bool isNew;
  Place place;

  PlaceDialog({required this.isNew, required this.place});

  Widget displayImage() {
    if (place.imageUrl != null) {
      return Image.file(File(place.imageUrl ?? ""));
    }
    return Container();
  }

  Widget buildDialog(BuildContext context) {
    DatabaseHandler dbHandler = DatabaseHandler();
    txtDes.text = place.designation ?? "";
    txtlat.text = place.latitude.toString();
    txtlon.text = place.longitude.toString();

    return AlertDialog(
      title: const Text("Place"),
      content: SingleChildScrollView(
        child: Column(
          children: [
            TextField(
              controller: txtDes,
              decoration: const InputDecoration(
                hintText: "Designation",
              ),
            ),
            TextField(
              controller: txtlat,
              decoration: const InputDecoration(
                hintText: "Latitude",
              ),
            ),
            TextField(
              controller: txtlon,
              decoration: const InputDecoration(
                hintText: "Longitude",
              ),
            ),
            IconButton(
                onPressed: () {
                  if (isNew) {
                    place.designation = txtDes.value.text;
                    place.longitude = double.parse(txtlon.value.text);
                    place.latitude = double.parse(txtlat.value.text);

                    dbHandler.insertPlace(place);
                  }
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => CameraPage(place: place)));
                },
                icon: const Icon(Icons.camera_front)),
            displayImage(),
            RaisedButton(
              onPressed: () {
                place.designation = txtDes.value.text;
                place.longitude = double.parse(txtlon.value.text);
                place.latitude = double.parse(txtlat.value.text);

                if (isNew) {
                  dbHandler.insertPlace(place);
                } else {
                  dbHandler.updatePlace(place);
                }
                Navigator.pop(context);
              },
              child: const Text("OK"),
            )
          ],
        ),
      ),
    );
  }
}
