import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:treasure_map/models/place.dart';
import 'package:treasure_map/pages/place_dialog.dart';
import 'package:treasure_map/pages/place_handler_page.dart';

import '../common/database_handler.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  static const CameraPosition position = CameraPosition(
    target: LatLng(50.611256, 3.134842),
    zoom: 12,
  );
  DatabaseHandler dbHandler = DatabaseHandler();
  late List<Marker> _markers = [];
  late List<Place> _places;

  Future<LatLng> _readCurrentPosition() async {
    Location location = Location();
    bool isServiceEnabled;
    PermissionStatus grantedPermission;
    LocationData locationData;

    isServiceEnabled = await location.serviceEnabled();
    if (!isServiceEnabled) {
      isServiceEnabled = await location.requestService();
      if (!isServiceEnabled) {
        return position.target;
      }
    }

    grantedPermission = await location.hasPermission();
    if (grantedPermission == PermissionStatus.denied) {
      grantedPermission = await location.requestPermission();
      if (grantedPermission == PermissionStatus.denied) {
        return position.target;
      }
    }

    locationData = await location.getLocation();
    return (LatLng(
      locationData.latitude ?? position.target.latitude,
      locationData.longitude ?? position.target.longitude,
    ));
  }

  void _addMarker(LatLng latLng, String id, String title) {
    Marker marker = Marker(
        markerId: MarkerId(id),
        icon: BitmapDescriptor.defaultMarkerWithHue((id == "currentPosition")
            ? BitmapDescriptor.hueAzure
            : BitmapDescriptor.hueOrange),
        position: latLng,
        infoWindow: InfoWindow(title: title));

    setState(() {
      _markers.add(marker);
    });
  }

  Future _readData() async {
    dbHandler.db = await dbHandler.openDb();
    _places = await dbHandler.getPlaces();
    for (var place in _places) {
      _addMarker(LatLng(place.latitude, place.longitude), place.id.toString(),
          place.designation ?? "");
    }
  }

  @override
  void initState() {
    super.initState();
    _readCurrentPosition().then(
        (value) => _addMarker(value, "currentPosition", "Position actuelle"));
    _readData();

    //dbHandler.loadPlace();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Treasure map"),
          actions: [
            IconButton(
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(
                          builder: (context) => const PlaceHandlerPage()))
                      .then((value) => {
                            _markers.clear(),
                            _readCurrentPosition().then((value) => _addMarker(
                                value, "currentPosition", "Position actuelle")),
                            _readData()
                          });
                },
                icon: const Icon(Icons.list)),
          ],
        ),
        body: GoogleMap(
          initialCameraPosition: position,
          markers: Set.of(_markers),
        ),
        floatingActionButton: Stack(
          children: [
            Positioned(
              left: 40,
              bottom: 40,
              child: FloatingActionButton(
                tooltip: "Add new place",
                child: const Icon(Icons.add_location),
                onPressed: () {
                  Marker marker = _markers.firstWhere(
                      (marker) =>
                          marker.markerId == const MarkerId("currentPosition"),
                      orElse: () => const Marker(
                          markerId: MarkerId("currentPosition"),
                          position: LatLng(0, 0)));
                  Place place = Place(
                      latitude: marker.position.latitude,
                      longitude: marker.position.longitude);
                  PlaceDialog dialog = PlaceDialog(isNew: true, place: place);
                  showDialog(
                      context: context,
                      builder: (context) {
                        return dialog.buildDialog(context);
                      }).then((value) => {
                        _markers.clear(),
                        _readCurrentPosition().then((value) => _addMarker(
                            value, "currentPosition", "Position actuelle")),
                        _readData()
                      });
                },
              ),
            )
          ],
        ));
  }
}
