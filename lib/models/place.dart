class Place {
  int? id;
  String? designation;
  double latitude;
  double longitude;
  String? imageUrl;

  Place(
      {this.id,
      this.designation,
      required this.latitude,
      required this.longitude,
      this.imageUrl});

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'designation': designation,
      'latitude': latitude,
      'longitude': longitude,
      'imageUrl': imageUrl,
    };
    if (id != null) {
      map['id'] = id;
    }
    if (imageUrl != null) {
      map['imageUrl'] = imageUrl;
    }
    return map;
  }

  Place.fromMap(Map<String, dynamic> map)
      : id = map['id'],
        designation = map['designation'],
        latitude = map['latitude'] * 1.0,
        longitude = map['longitude'] * 1.0,
        imageUrl = map['imageUrl'];
}
